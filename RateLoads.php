<?php

namespace Layers\Processor\Entry;

use \Zeus\LoadFusion\Rating\RateCalculator,
		\Zeus\LoadFusion\Models\Constraints;

/**
 * The RateLoads Cl
 * @category	 Entry
 * @author     ddirin
 * @since      Release 2.2
 */
class RateLoads extends \Layers\Entry {

	protected $constraints;

	/**
	 * Need and instance of Constraints
	 * @param \Zeus\LoadFusion\Models\Constraints $constraints
	 */
	public function __construct( Constraints $constraints ) {
		$this->constraints = $constraints;
		$this->locSB = &parent::$engSB[get_parent_class( __CLASS__ )];
		$this->locSB['LTL-RATES'] = array();
	}

	/**
	 * Gets LTL and TL rates for orders. It determines based on price which freight
	 * rate it should be rated as. So if LTL is cheaper it will be an LTL
	 * if TL is cheaper it will be a TL.
	 * 
	 * @todo: At some point we want to implement some 'Break Weight' concepts
	 * @todo: We would need a constraint for a max ltl weight and a min Truckload rate
	 * @param type $orders
	 */
	public function rateLoads( &$orders ) {
		$ltlRates = $this->constraints->ltlEnabled ? $this->rateLTLs( $orders ) : FALSE;
		$truckLoadRates = $this->rateTruckLoads( $orders );
		$length = count( $orders );
		for( $i = 0; $i < $length; $i++ ) {
			//the truckload rate is subject to minimum charge
			$tl = max( $this->constraints->minimumCharge, $truckLoadRates[$i] );
			if( $ltlRates && $ltlRates[$i] < $tl ) {
				$orders[$i]['order_freight_type'] = 'LTL';
				$orders[$i]['order_freight_rate'] = $ltlRates[$i];
			} else {
				$orders[$i]['order_freight_type'] = 'TL';
				$orders[$i]['order_freight_rate'] = $tl;
			}
			//update orders with rate
			parent::$engSB['ENGINE']['db']->update( 'opt_tmp_dat_order_parameters', array(
				'param_order_number' => $orders[$i]['order_number'], 'import_id' => IMPORT_ID ), array(
				'order_freight_type' => $orders[$i]['order_freight_type'], 'order_freight_rate' => $orders[$i]['order_freight_rate'] ) );

			//store ltl rate for use late
			$this->locSB['LTL-RATES'][$orders[$i]['param_order_number']] = $ltlRates[$i];
		}
	}

	/**
	 * Get LTLs for existing rates
	 * @param array $orders
	 * @return array
	 */
	public function rateLTLs( array $orders ) {
		$ltlLookupArray = array();
		foreach( $orders as $order ) {
			$ltlLookupArray[] = array(
				'origin_zip' => $order['order_ogn_zip'],
				'destination_zip' => $order['order_dest_zip'],
				'order_weight' => $order['order_weight'],
				'order_date' => date( 'Ymd', $order['order_date1'] ),
				'carrier_type' => $this->constraints->EQUIP[$order['order_fleet']]['CARRIER_TYPE']
			);
		}
		return RateCalculator::calculateMultipleLTLs( $ltlLookupArray, $this->constraints->RATE_LTL_DISCOUNT );
	}

	/**
	 * Rates Truck Loads for given orders
	 * @param array $orders
	 * @return type
	 */
	public function rateTruckLoads( array $orders ) {
		$rates = array();
		foreach( $orders as $key => $order ) {
			$route = new \Zeus\LoadFusion\Models\Data\Route();
			if( $this->constraints->emptyMiles && $this->constraints->modelType === 'IN' ) {
				$route->addStop( array(
					'routing_location_code' => $order['order_dest_code'],
					'routing_ogndest_miles' => 0,
					'routing_location_type' => 'EM'
				) );
			}
			$route->addStop( array(
				'routing_location_code' => $order['order_ogn_code'],
				'routing_ogndest_miles' => $this->constraints->modelType === 'IN' && $this->constraints->emptyMiles ? $order[MILEAGE_CACHE_COLUMN] : 0,
				'routing_location_type' => 'PU'
			) );

			$route->addStop( array(
				'routing_location_code' => $order['order_dest_code'],
				'routing_location_type' => 'DR',
				'routing_ogndest_miles' => $order[MILEAGE_CACHE_COLUMN],
				'routing_weight' => $order['order_weight'],
			) );
			if( $this->constraints->emptyMiles && $this->constraints->modelType === 'OUT' ) {
				$route->addStop( array(
					'routing_location_code' => $order['order_ogn_code'],
					'routing_ogndest_miles' => $order[MILEAGE_CACHE_COLUMN],
					'routing_location_type' => 'EM'
				) );
			}

			$route['routing_direction'] = $this->constraints->modelType;
			$route['routing_numstops'] = count( $route->getStops( false ) ) - 1;
			$rateCalc = new RateCalculator( $route );
			$rateCalc->carrierType = $this->constraints->EQUIP[$order['order_fleet']]['CARRIER_TYPE'];

			try {
				$rates[] = $rateCalc->rateTruckLoad();
			} catch( \Exception $e ) {
				$rates[] = $this->constraints->minimumCharge;
			}
		}
		return $rates;
	}

}
